import { action_types, constants } from "../common/constants";

const initialState = {
  LoginReducer: {
    githubCode: "",
    accessToken: "",
    tokenType: ""
  }
};

export default function LoginReducer(state = initialState, action) {
  const newState = { ...state };
  switch (action.type) {
    case action_types.redirect:
      const url = `https://github.com/login/oauth/authorize?client_id=${
        constants.clientId
      }&scope=${constants.email}`;

      window.location.href = url;

      break;

    case action_types.getAccessTokenSuccess:
      newState.LoginReducer.accessToken = action.payload;
      break;

    default:
      return newState;
  }

  return newState;
}
