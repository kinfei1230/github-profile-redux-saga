import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import reduxStore from "./store/reduxStore";
import CallBack from "./containers/CallBack";
import Profile from "./containers/HomeContainer";
import UserRepo from "./containers/RepoContainer";
//import registerServiceWorker from "./registerServiceWorker";

import { Provider } from "react-redux";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import NotFound from "./components/NotFound/NotFoundComponent";

import "./index.css";

ReactDOM.render(
  <Provider store={reduxStore}>
    <BrowserRouter>
      <Switch>
        <Route exact path="/github/callback" component={CallBack} />
        <Route exact path="/home" component={Profile} />
        <Route exact path="/repo" component={UserRepo} />
        <Route path="/not-found" component={NotFound} />
        <Route exact path="/" component={App} />
        <Redirect to="/not-found" />
      </Switch>
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);

//registerServiceWorker();
