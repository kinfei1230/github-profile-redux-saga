import React, { Component } from "react";
import { connect } from "react-redux";
import { constants, action_types } from "../common/constants";
import { Redirect } from "react-router-dom";
import { generals } from "../common/constants";

import store from "store2";
import queryString from "query-string";

class CallBack extends Component {
  constructor(props) {
    super(props);
    this.dispatch = props.dispatch;
  }

  componentDidMount() {
    const { accessToken } = this.props;
    const { code } = queryString.parse(window.location.search);
    console.log(code);
    if (accessToken === "" || accessToken === undefined) {
      const params = {
        client_id: constants.clientId,
        client_secret: constants.clientSecret,
        code: code
      };

      const url = `/login/oauth/access_token?${queryString.stringify(params)}`;

      this.dispatch({
        type: action_types.getAccessToken,
        payload: {
          params: url
        }
      });
    }
  }
  render() {
    let access_token = store.get(generals.access_token);
    if (access_token) {
      return <Redirect to="/home" />;
    }

    return <div />;
  }
}

function mapStateToProps(state) {
  return { ...state };
}

export default connect(mapStateToProps)(CallBack);
