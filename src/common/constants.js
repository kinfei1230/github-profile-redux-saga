export const constants = {
  clientId: "a1687788017857975154",
  clientSecret: "5e297fcd0aeb3b3186b1f9aac100ca481a5b9482",
  email: "keafea@hotmail"
};

export const action_types = {
  redirect: "redirect",
  getAccessToken: "getAccessToken",
  getAccessTokenSuccess: "getAccessTokenSuccess",
  getUserProfile: "getUserProfile",
  getUserProfileSuccess: "getUserProfileSuccess",
  getUserRepo: "getUserRepo",
  getUserRepoSuccess: "getUserRepoSuccess",
  error: "error"
};

export const generals = {
  access_token: "access_token"
};
