import { action_types, generals } from "../common/constants";
import store from "store2";

export const onLogin = () => {
  return {
    type: action_types.redirect
  };
};

export const getUserProfile = () => {
  let access_token = store.get(generals.access_token);
  const url = `/user?access_token=${access_token}`;

  return {
    type: action_types.getUserProfile,
    payload: {
      params: url
    }
  };
};

export const getUserRepo = () => {
  let access_token = store.get(generals.access_token);
  const url = `/user/repos?access_token=${access_token}`;

  return {
    type: action_types.getUserRepo,
    payload: {
      params: url
    }
  };
};
