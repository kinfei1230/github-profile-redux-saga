import React, { Component } from "react";
import { generals } from "../../common/constants";
import Button from "@material-ui/core/Button";
import store from "store2";
import PropTypes from "prop-types";

// React component
export default class LoginComponent extends Component {
  static contextTypes = {
    router: PropTypes.object
  };

  componentDidMount() {
    let access_token = store.get(generals.access_token);
    if (access_token) {
      this.context.router.history.push(`/home`);
    }
  }

  render() {
    const { onLoginClick } = this.props;
    return (
      <div className="">
        <div>
          <h1>Github Profile Test</h1>
        </div>
        <div>
          <Button variant="outlined" color="primary" onClick={onLoginClick}>
            Login Github
          </Button>
        </div>
      </div>
    );
  }
}
