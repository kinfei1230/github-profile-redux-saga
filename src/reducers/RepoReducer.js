import { action_types } from "../common/constants";

const initialState = {
  RepoReducer: {
    repoData: [],
    message: ""
  }
};

export default function RepoReducer(state = initialState, action) {
  const newState = { ...state };
  switch (action.type) {
    case action_types.getUserRepoSuccess:
      return Object.assign({}, newState.RepoReducer, {
        repoData: action.payload
      });

    case action_types.error:
      return Object.assign({}, newState.HomeReducer, {
        message: action.message
      });

    default:
      return newState;
  }
}
