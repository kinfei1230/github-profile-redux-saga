import React, { Component } from "react";
import NavBarComponent from "../Navbar/NavBarComponent";

export default class NotFound extends Component {
  render() {
    return (
      <div>
        <NavBarComponent />
        <div style={{ padding: 24, paddingTop: 0 }}>
          <h1>404 Page not found</h1>
        </div>
      </div>
    );
  }
}
