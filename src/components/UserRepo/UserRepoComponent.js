import React, { Component } from "react";
import NavBarComponent from "../Navbar/NavBarComponent";
import Moment from "moment";
import CardContents from "@material-ui/core/Card";
import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

// React component
export default class UserRepoComponent extends Component {
  displayData() {
    Moment.locale("en_MY");
    return this.props.repo.map((x, i) => {
      return (
        <div key={i}>
          <Grid container spacing={24} style={{ padding: 24, paddingTop: 0 }}>
            <Grid item xs={12} sm={12} lg={12} xl={12}>
              <Card>
                <CardContents style={{ padding: 24 }}>
                  <Typography component="p">
                    Repository name: {x.full_name}
                  </Typography>
                  <Typography component="p">
                    Description: {x.description}
                  </Typography>
                  <Typography component="p">
                    Created: {Moment(x.created_at).format("LLL")}
                  </Typography>
                  <Typography component="p">Language: {x.language}</Typography>
                  <Typography component="p">
                    Watcher(s): {x.watchers_count}
                  </Typography>
                  <Typography component="p">Fork(s): {x.forks}</Typography>
                  <Typography component="p">
                    <a
                      href={x.html_url}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      View Repo
                    </a>
                  </Typography>
                </CardContents>
              </Card>
            </Grid>
          </Grid>
        </div>
      );
    });
  }

  render() {
    const { repo } = this.props;

    if (repo) {
      return (
        <div>
          <NavBarComponent />
          <div>{this.displayData()}</div>
        </div>
      );
    }

    return (
      <div>
        <NavBarComponent />
        <div>
          <Grid container spacing={24} style={{ padding: 24 }}>
            <Grid item xs={12} sm={12} lg={12} xl={12}>
              <Card>
                <CardContents style={{ padding: 24 }}>
                  <Typography component="p">No data.</Typography>
                </CardContents>
              </Card>
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}
