import { connect } from "react-redux";
import LoginComponent from "../components/Login/LoginComponent";
import * as actions from "../actions";

// Map Redux state to component props
function mapStateToProps(state) {
  return {
    ...state
  };
}

// Map Redux actions to component props
function mapDispatchToProps(dispatch) {
  return {
    onLoginClick: () => dispatch(actions.onLogin())
  };
}

// Connected Component
const LoginContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginComponent);

export default LoginContainer;
