import { call, put, takeLatest, all } from "redux-saga/effects";
import { generals, action_types } from "../common/constants";
import { fetchAccessToken, fetchUserInformation, fetchUserRepo } from "./api";

import store from "store2";

function* getAccessToken(action) {
  try {
    const access_token = yield call(fetchAccessToken, action.payload.params);

    if (access_token) {
      store.set(generals.access_token, access_token);
      yield put({
        type: action_types.getAccessTokenSuccess,
        payload: access_token
      });
    }
  } catch (e) {
    yield put({
      type: action_types.error,
      message: "Unable to fetch access token"
    });
  }
}

function* getUserProfile(action) {
  try {
    const json = yield call(fetchUserInformation, action.payload.params);
    yield put({
      type: action_types.getUserProfileSuccess,
      payload: json
    });
  } catch (e) {
    yield put({
      type: action_types.error,
      message: "Unable to fetch user information"
    });
  }
}

function* getUserRepo(action) {
  try {
    const json = yield call(fetchUserRepo, action.payload.params);
    yield put({
      type: action_types.getUserRepoSuccess,
      payload: json
    });
  } catch (e) {
    yield put({
      type: action_types.error,
      message: "Unable to fetch user repo"
    });
  }
}

function* actionWatcher() {
  yield takeLatest(action_types.getAccessToken, getAccessToken);
  yield takeLatest(action_types.getUserProfile, getUserProfile);
  yield takeLatest(action_types.getUserRepo, getUserRepo);
}

export default function* rootSaga() {
  yield all([actionWatcher()]);
}
