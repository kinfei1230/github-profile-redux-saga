import { action_types } from "../common/constants";

const initialState = {
  HomeReducer: {
    userData: null,
    message: ""
  }
};

export default function HomeReducer(state = initialState, action) {
  const newState = { ...state };
  switch (action.type) {
    case action_types.getUserProfileSuccess:
      return Object.assign({}, newState.HomeReducer, {
        userData: action.payload
      });

    case action_types.error:
      return Object.assign({}, newState.HomeReducer, {
        message: action.message
      });

    default:
      return newState;
  }
}
