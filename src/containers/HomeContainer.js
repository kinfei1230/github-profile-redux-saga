import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import HomeComponent from "../components/Home/HomeComponent";
import * as actions from "../actions";

class Profile extends Component {
  componentDidMount() {
    this.props.onGetUserProfile();
  }

  render() {
    return (
      <div>
        <HomeComponent profile={this.props.data} />
      </div>
    );
  }
}

// Map Redux state to component props
const mapStateToProps = ({ HomeReducer }) => {
  return {
    data: HomeReducer.userData
  };
};

// Map Redux actions to component props
function mapDispatchToProps(dispatch) {
  return {
    onGetUserProfile: () => dispatch(actions.getUserProfile())
  };
}

Profile.propTypes = {
  onGetUserProfile: PropTypes.func
};

// Connected Component
const HomeContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);

export default HomeContainer;
