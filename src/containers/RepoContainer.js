import React, { Component } from "react";
import { connect } from "react-redux";
import UserRepoComponent from "../components/UserRepo/UserRepoComponent";
import * as actions from "../actions";

class UserRepo extends Component {
  componentDidMount() {
    this.props.onGetUserRepo();
  }

  render() {
    return (
      <div>
        <UserRepoComponent repo={this.props.data} />
      </div>
    );
  }
}

// Map Redux state to component props
const mapStateToProps = ({ RepoReducer }) => {
  return {
    data: RepoReducer.repoData
  };
};

// Map Redux actions to component props
function mapDispatchToProps(dispatch) {
  return {
    onGetUserRepo: () => dispatch(actions.getUserRepo())
  };
}

// Connected Component
const RepoContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserRepo);

export default RepoContainer;
