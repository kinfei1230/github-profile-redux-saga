import React, { Component } from "react";
import NavBarComponent from "../Navbar/NavBarComponent";
import CardContents from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

// React component
export default class HomeComponent extends Component {
  displayData() {
    const { avatar_url, name, company, blog, bio } = this.props.profile;
    return (
      <div>
        <Grid container spacing={24} style={{ padding: 24, paddingTop: 0 }}>
          <Grid item xs={12} sm={6} lg={4} xl={3}>
            <Card>
              <CardMedia
                style={{ height: 0, paddingTop: "56.25%" }}
                image={avatar_url}
                title="avatar"
              />
              <CardContents style={{ padding: 24 }}>
                <Typography component="p">Name: {name}</Typography>
                <Typography component="p">Company: {company}</Typography>
                <Typography component="p">Blog: {blog}</Typography>
                <Typography component="p">Bio: {bio}</Typography>
              </CardContents>
            </Card>
          </Grid>
        </Grid>
      </div>
    );
  }

  render() {
    const { profile } = this.props;
    if (profile) {
      return (
        <div>
          <NavBarComponent />
          <div>{this.displayData()}</div>
        </div>
      );
    }
    return (
      <div>
        <NavBarComponent />
        <div>No data.</div>
      </div>
    );
  }
}
