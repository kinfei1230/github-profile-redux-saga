import { combineReducers } from "redux";
import LoginReducer from "./LoginReducer";
import HomeReducer from "./HomeReducer";
import RepoReducer from "./RepoReducer";

const reducer = combineReducers({
  LoginReducer,
  HomeReducer,
  RepoReducer
});

export default reducer;
